
## GOAL
1. interview people I know and collegeas at random locations with as a goal
    1. share experiences and insights from people (focus on Business, Enterprise Architecture & Enterprise Engineering)
    1. learn what is relevant for recording and having conversations
1. learn how to record a podcast
    1. learn what is of influence of the "warmth" of a voice (type, brand of mic, standing)
    1. learn what is of influence of the surrounding noise
    1. learn how quickly you need to balance the audio before the recording (amplifier/mixer)
1. creating a podcast (non-technical)
    1. learn the importance of a script
    1. learn the importance of preperation
1. on the road
    1. the kit should be small and fit in my backpack
    1. the kit should be low on power or even just a few batteries
    1. the kit should be quick to setup with just a few chairs and table optional
    1. the surroundings of the interview will vary so sound of the surroundings is to be expected.
1. Inspired by the list of Dan Andrews
    1. **Must be able to record 1-2 person conversations.**, I think I will start with just interviewing people, and ask two people to read out some prepared texts.
    1. **Must be as impervious as possible to noise from motorcycles, traffic, air conditioners, crowing roosters, etc.**, true that. Interviews will be at offices, at people homes and in restaurants.
    1. **Must be rugged.**, true that. I have the tendency to kill stuff with ease. And I run around with a backpack all the time.
    1. **Must be easy to use and set up.**
    1. **Must fit into my personal hand luggage and not be too heavy.** (see the rugged option)
    1. **Must allow podcasters to monitor their performance in real time.**, this is very helpfull since I have not that much time to do post processing.
    
# CONTENT / ToC / LoC

# My kit
I decided to go for the audio recorder Zoom H6. Since this module already had a great microphone included, and supports future expansion to one or more XLR microphone.
A few people convinced me that:
1. The Zoom H6 appears to support record in high quality speech (reading) 
1. The Zoom H6 is way better then the Zoom h4 and the difference is about 60 EUR
1. The Zoom H1 wil not support XLR or multitrack and thus will only work with 2 laveliers mics and then two mono tracks seperate on the left and right track records. and that is then all you can do. PS if you want to record one or two lavelier tracks you can do with Zoom H1 or the Zoom F1N.
1. The Zoom H6 expanded with an lavelier mic enables me with the onboard microphone to record high quality interviews.
1. This setup supports the option to add more levaliers mics in future or add seperate XLR/USB mics when I want to go further in the reading route.


1. Audio Recorder (285 EUR)
    1. **Zoom H6** ([bax-shop.nl](https://www.bax-shop.nl/zoom-h6))
1. Headphone(s) (budget:approx 20 EUR each)
    1. headphone 1 - **Sony MDR-ZX110 Wit**(14 EUR) ([tweakers-pricewatch](https://tweakers.net/product/410924/sony-mdr-zx110/))
1. Microphone (24.40 EUR)
    1. Audio Technica ATR3350 lavalier microfoon ([bax-shop.nl](https://www.bax-shop.nl/dasspeld/audio-technica-atr3350-lavalier-microfoon))
1. Microphone table stand (24.90 EUR)
    1. Zoom Manfrotto Pixi tripod ([bax-shop.nl](https://www.bax-shop.nl/recorder-mounts/zoom-manfrotto-pixi-driepoot))
1. Adapters
    1.  Multitple headphones (0.60 EUR)
        1. HQ Adapter plug 3.5mm stereo stekker - 2x 3.5mm stereo kontra ([bax-shop.nl](https://www.bax-shop.nl/jack-jack/hq-adapter-plug-3.5mm-stereo-stekker-2x-3.5mm-stereo-kontra/product-details.html)) 
    1. ATR3350 2 Zoom H6 (1.50 EUR + 0.65 EUR)
        1. HQ plug 3.5mm female stereo naar 6.35mm male stereo adapter ([bax-shop.nl](https://www.bax-shop.nl/koptelefoon-verloopadapter/hq-plug-3.5mm-female-stereo-naar-6.35mm-male-stereo-adapter/product-details.html))
        1. HQ adapter plug 6.35mm mono male - 3.5mm mono female ([bax-shop.nl](https://www.bax-shop.nl/jack-jack-verlooppluggen/hq-adapter-plug-6-35mm-mono-male-3-5mm-mono-female))

# Tips and Tricks on the PodCast content
1. [How To Ask Good Questions Without Being A Jerk by podcastmotor](https://www.podcastmotor.com/ask-good-questions/)


## Backlog
1. headphone 2 - **Behringer HPX2000** (19.99)  ([tweakers-pricewatch](https://tweakers.net/pricewatch/403030/behringer-hpx2000-(zilver-zwart).html))
1. External microphone(s) (budget: approx 50 EUR each)
    1. **Audio-Technica ATR2100** (? EUR, 70 USD) ([Amazon.com](https://www.amazon.com/Audio-Technica-ATR2100-USB-Cardioid-Dynamic-Microphone/dp/B004QJOZS4?th=1))
    1. **Samson Q2U** (75 EUR, 55 USD) ([tweakers-pricewatch](https://tweakers.net/pricewatch/unsorted/840710/samson-q2u-usb-xlr-dynamische-mikrofon-inklusive-hp20-headphones.html), [Amazon.com](https://www.amazon.com/Samson-Handheld-Microphone-Recording-Podcasting/dp/B001R747SG))
1. Cable: 
    1. mic<>recorder (XLR -> jack) - (2*5 EUR) [bax-shop](https://www.bax-shop.nl/verlooppluggen-kabels/xlr-jack)
    1. 2 jacks to 1 jack - **Hosa YMM261 Y-Cable 3.5 mm TRS 3.5 mm TSF** (8 EUR) [amazon.de](https://www.amazon.de/Hosa-YMM261-Y-Kabel-5mm-TRS-3/dp/B000068O5H/ref=sr_1_fkmr0_1?ie=UTF8&qid=1534359576&sr=8-1-fkmr0&keywords=hose+ymm-261)
    1. headphone splitter - **Belkin Rockstar** (14 EUR) [pricewatch](https://tweakers.net/pricewatch/390435/belkin-rockstar.html)[amazon.de](https://www.amazon.de/dp/B00BE69DSE/ref=asc_df_B00BE69DSE54810913/?tag=pricewatch-21&amp&creative=22398&creativeASIN=B00BE69DSE&linkCode=df0&language=nl_NL)
1. Microphone  Popfilter (budget: approx 10 EUR)
    1. ?

# KITS - which mic for a beginner
## Main inspiration for the list
1. [How to Build a Portable Podcast Studio BY:  Dan Andrews, 10.27.2015](http://www.tropicalmba.com/portable-podcast-studio/) , this is the main inspiration on the goal and setup.
1. [How To Make Audio That Doesn’t Suck: The No-Nonsense Starter Guide For Podcast And Audio Course
by Natasha Che, January 22nd 2018](https://mysoundwise.com/blog/post/how-to-make-audio-that-doesnt-suck-the-no-nonsense-starter-guide-for-podcast-and-audio-course), this is my double check on the content of the kit on parts to be included.
1. [How to Build a Podcast Studio on Any Budget by Nathan Fraser](https://podcastblastoff.com/post/How-to-Build-a-Podcast-Studio-on-Any-Budget), great extensive list with after some research convirmed great picks.
1. [The Podcastersstudio Gear list by Ray Ortega](http://thepodcastersstudio.com/gear/), great list with nice examples. decided to skip the mixer part.
1. [The Best Podcast Starter Kit (For 1, 2, 3 & 4+ Hosts), JULY 25, 2018 / ROSS](https://www.podcastinsights.com/podcast-starter-kit/), great list of parts and selection, not all parts are budget.

## Just some lists of products.
### Micrphones
1. [The Best USB Computer Microphones for Home Recording 2018](https://ehomerecordingstudio.com/usb-microphones/), just a list of USB microphones.
1. [THE BEST PODCAST MICROPHONES ON THE MARKET Posted by Colin Gray | Nov 7, 2017](https://www.thepodcasthost.com/equipment/the-best-podcasting-microphones-on-the-market/)
1. [177: Gear Review: Samson Q2U Recording & Podcasting Pack, by Michael Murphy Jul 31, 2017](https://medium.com/@mikemurphyco/177-gear-review-samson-q2u-recording-podcasting-pack-a86852827f0)
1. [25 of the Best Podcast Microphones by KEVIN GOLDBERG, JANUARY 11, 2018 8:24 AM](https://discoverpods.com/best-podcast-mic-podcasting-microphone/), list of mics
1. [Best XLR Microphones 🎙️ For Podcasting & Voice (For Every Budget) 2018  MAY 30, 2018 / ROSS ](https://www.podcastinsights.com/best-xlr-microphones/), nice list of mics

### Recorders
1. [THE BEST DIGITAL PODCAST RECORDERS ON THE MARKET
Posted by Colin Gray May 15, 2017](https://www.thepodcasthost.com/equipment/best-digital-podcast-recorders/)

### Kits
1. [3 Options for Minimal and Portable Podcasting Gear – TAP317 by Daniel J. Lewis, August 8, 2017 ](https://theaudacitytopodcast.com/3-options-for-minimal-and-portable-podcasting-gear-tap317/)
1. [How to Build a Portable Podcast Studio
by Bryan Entzmingerm Oct 30, 2015](https://engagingmissions.com/how-to-build-a-portable-podcast-studio/)
1. [The Best Mobile Podcasting Equipment by CHRIS LUECKE, NOVEMBER 29, 2017](http://www.chrisluecke.com/blog/best-mobile-podcasting-equipment), good kit list for the parts defintion, not so nice for a beginners equipment list
1. [The Absolute Beginners Guide to Podcasting: Equipment  by Jeff Large	on July 27, 2018](https://www.ostraining.com/blog/podcasting/equipment/), just a list with expensive stuff
1. [The Complete List of Audio Gear You'll Need for Your Podcast
by Adrian Try27 Sep 2013](https://music.tutsplus.com/articles/the-complete-list-of-audio-gear-youll-need-for-your-podcast--audio-20527), just another list
1. [The Best Podcast Equipment for Professional Podcast Studio Setup Published by  Salman Aslam at  January 11, 2018 Categories ](https://www.omnicoreagency.com/best-podcast-equipment/), nice list for a full kit
1. [The Best Podcasting Equipment for Pitch Perfect Recordings By Brad Smith - June 13, 2018](https://www.digitalexaminer.com/best-podcasting-equipment/), great list
1. [Podcasting Basics, Part 1: Voice Recording Gear by Jeff Towne   |  3.20.15](https://transom.org/2015/podcasting-basics-part-1-voice-recording-gear/), nice list way to expensive for budget or beginners
1. [The 10 Technology Items You'll Need to Start a Podcast, Brenton Hayden, March 16, 2016 ](https://www.entrepreneur.com/article/271391), great parts list for podcasts kit

### How to make a podcast (non-technical)
1. [How to make a podcast Everything you need to know to make it shine By Andy Huang](http://www.audiocraft.com.au/how-to-make-a-podcast/#sound-good), nice story
1. [Audio Recording for Digital Storytelling by © JISC Netskills 2014](https://warwick.ac.uk/fac/arts/classics/staff/rowan/digitalstorytelling/netskills_tm563_563_digital_storytelling-01_audio_recording_for_ds_pe_share.pdf)
 
# Research
## Audio Recorder
### Reason
### Options
* Zoom H1.
* Tascam DR-05. 
* Roland R-05.
* Zoom H2n

1. https://www.google.nl/search?q=ZOOM+H1+Handy+Portable+Digital+Recorder+for+audio+podcast
1. https://www.zoom-na.com/products/field-video-recording/field-recording/zoom-h1n-handy-recorder#specs
1. https://www.diyphotography.net/3-zoom-h1-recording-tips-i-learned-the-hard-way/

## Headphone(s)
### Reason
### Options
* Bose QC 15’s 
* Audio-Technica ATH-M30x
1. https://www.amazon.de/Audio-Technica-ATH-M30X-DJ-Kopfh%C3%B6rer-Studio/dp/B00HVLUQW8/ref=sr_1_1?ie=UTF8&qid=1534063217&sr=8-1&keywords=Audio-Technica+ATH-M30x


## External Microphone(s)
### Reason
### Options
1. [Audio-Technica ATR2100](https://www.amazon.com/gp/product/B01HOYG0LW/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=B01HOYG0LW&linkCode=as2&tag=debatemotions-20&linkId=30f8a17d1ca63ca2aac8bae19f596ec3)
1. Audio-Technica AT2020USB+. (expensive)
1. Audio-Technica AT2005USB 
1. [Samson Q2U](https://www.thepodcasthost.com/equipment/samson-q2u-podcasting-review/)
1. [Behringer Ultravoice Xm8500 Dynamic Vocal Microphone, Cardioid](https://www.amazon.com/Behringer-Ultravoice-Dynamic-Microphone-Cardioid/dp/B0002KZAKS/ref=pd_lpo_vtph_267_bs_lp_t_1?_encoding=UTF8&psc=1&refRID=31KNJP4VHYBPHKP5CWV3#customerReviews
)
1. Audio-Technica PRO31QTR Cardioid Dynamic Microphone
1. AKG D5 Dynamic Handheld Vocal Microphone

## Cable(s)
### Reason
### options

#### XLR Cables
1. https://www.amazon.com/gp/product/B0002GML68/ref=as_li_tl?ie=UTF8&tag=t0de96-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=B0002GML68&linkId=0805b28877a879190e61be2097aebadd

#### Splitter
1. https://www.amazon.com/Belkin-Rockstar-Multi-Headphone-Splitter/dp/B00904WS2K/

#### two mics to one input
1. https://nofilmschool.com/2017/11/connect-two-mics-your-camera-5-cable
1. https://www.thepodcasthost.com/equipment/using-zoom-h1-2-lavalier-mics/


## Microphone Popfilter
### Reason
### Options
1.  https://www.amazon.com/dp/B06XJ19VWG/ref=cm_sw_r_cp_ep_dp_5OszAb7W65DE6
1.  https://www.amazon.com/gp/product/B0002GXF8Q/ref=as_li_tl?ie=UTF8&tag=t0de96-20&camp=1789&creative=9325&linkCode=as2&creativeASIN=B0002GXF8Q&linkId=2baa734653a25061d32d9500babc9512


## Microphone table stand
### Reason
### Options
1. https://www.amazon.com/dp/B00OZ9C9LK/ref=cm_sw_r_cp_ep_dp_KVszAbZ0WZJ1F


## Amplifiers and Mixers
* AMP
* BEHRINGER MICROAMP HA400
* NW-100

## Audio terminilogy

### microphone Connector
1. USB input/output
1. XLR input/output

### microphone type
1. dynamisch (dynamic)
1. condensator
1. Condenser Mics
1. Dynamic Mics
1. Ribbon Mics
1. Stereo Mics

### microphone polarity
1. Cardioid
1. Figure-8
1. Omnidirectional
1. X/Y stereo 
1. STEREO
1. cardioid
1. omni
1. figure-8







